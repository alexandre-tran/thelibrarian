# thelibrarian

## Description

Code challenge to write small console application for borrowing, returning book to a library and check the remaining stock.

[Challenge Description](https://github.com/gar/library_code_challenge)

This project has been developed using TDD technique.

Code coverage report can be seen by clicking on [![coverage report](https://gitlab.com/alexandre-tran/thelibrarian/badges/master/coverage.svg)](https://alexandre-tran.gitlab.io/thelibrarian/coverage) badge.

## Dependencies

This project run and tested with:

* [nodejs v12.19.0](https://nodejs.org/uk/blog/release/v12.19.0/)
* [npm v6.14.8](https://www.npmjs.com/package/npm/v/6.14.8)

This project use:

* [mocha](https://mochajs.org/) for unit testing
* [nyc](https://www.npmjs.com/package/nyc) for code coverage
* [eslint](https://eslint.org/) for code quality
* [jsdoc](https://jsdoc.app/) for documentation


## Documentation

[Documentation](https://alexandre-tran.gitlab.io/thelibrarian/) _(generated with jsdoc)_

## Run demo

1. Clone the repo
1. Install dependencies by running ```npm install```
1. Run ```npm run demo``` to see output

The demo is also running in DemoTest as part of the [pipeline](https://gitlab.com/alexandre-tran/thelibrarian/pipelines/master/latest).

## Run Unit Test

1. Clone the repo (if not already)
1. Install dependencies by running ```npm install``` (if not already)
1. Run ```npm run test:unit```