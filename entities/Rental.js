const Inventory = require('./Inventory');

/**
 * Rental management system
 */
class Rental extends Inventory {
  /**
   * Constructor
   */
  constructor() {
    super();
    this._own = {};
  }

  /**
   * Add item to inventory
   * @param {any} item Item to add to inventory
   * @param {number} number The number of copy to add
   * @return {Inventory} return this for chaining
   */
  add(item, number = 1) {
    try {
      super.add(item, number);
      const reference = this._getItemReference(item);

      if (!this._own.hasOwnProperty(reference)) {
        this._own[reference] = 0;
      }

      this._own[reference] += number;
    } catch (ex) {
      throw new Error('Unable to add item to stock');
    }

    return this;
  }

  /**
   * Rent an item
   * Deduct from the stock
   * @param {any} item
   * @return {Rental} return this for chaining
   */
  rent(item) {
    super.deduct(item);
    return this;
  }

  /**
   * When item is return to the stock
   * @param {any} item
   * @return {Rental} return this for chaining
   */
  return(item) {
    super.add(item);

    const reference = this._getItemReference(item);

    if (super.stock(reference) > this._own[reference]) {
      this._own[reference] = super.stock(reference);
    }

    return this;
  }

  /**
   * Get stock for item provided.
   * If item not provided, return full stock
   * @param {any} item Optional - Item to search for
   * @return {array|numeric} If no item provide, retrieve list
   * otherwise, provide the stock for the provided item
   */
  stock(item = null) {
    // If no arguments provided full list
    if (item === null) {
      const items = [];

      for (const key in this._stock) {
        if (this._stock.hasOwnProperty(key)) {
          items.push({
            reference: key,
            stock: this._stock[key],
            own: this._own[key],
          });
        }
      }

      return items;
    }

    try {
      const reference = this._getItemReference(item);

      const itemInfo = {
        reference: reference,
        stock: 0,
        own: 0,
      };

      if (this._stock.hasOwnProperty(reference)) {
        itemInfo.stock = this._stock[reference];
        itemInfo.own = this._own[reference];
      }

      return itemInfo;
    } catch (ex) {
      throw new Error('Unable to peform search for item provided');
    }
  }
}

module.exports = Rental;
