const Book = require('./Book');
const Rental = require('./Rental');

/**
 * Class library allowing storing collection
 * of Books
 * @see Book
 */
class Library {
  /**
   * Constructor
   * @param {any}       data      List book information (CSV, array...)
   * @param {Inventory} inventory Object managing inventory
   * @param {function}  writer    Function to output log
   */
  constructor(data, inventory = null, writer = null) {
    if (inventory !== null) {
      if (!(inventory instanceof Rental)) {
        throw new Error('Rental management must be type Rental');
      }
    }

    if (writer !== null) {
      if (typeof writer !== 'function') {
        throw new Error('Writer must be a function callback.');
      };
    }

    this._collection = {};
    this._writer = writer;
    this._inventory = inventory;

    if (typeof data === 'string') {
      return this._loadFromCSV(data);
    }

    if (Array.isArray(data)) {
      return this._loadFromArray(data);
    }

    throw new Error('Library only load data from Array or CSV.');
  }

  /**
   * PRIVATE
   */

  /**
   * Load data from array
   * @private
   * @param {array} data Array of books
   * @return {Library} Return this for chaining
   */
  _loadFromArray(data) {
    for (const book of data) {
      if (!Book.validateISBN(book.ISBN)) {
        throw new Error('Invalid ISBN');
      }

      this._collection[book.ISBN] = new Book(book.ISBN
          , book.name
          , book.author
          , book.published);

      if (this._inventory !== null) {
        this.add(book.ISBN, (book.hasOwnProperty('stock') ? book.stock : 0));
      }
    }

    return this;
  }

  /**
   * Load data from CSV
   * @private
   * @param {CSV} data  CSV format: ISBN,name,author,publication year
   * @return {Library} Return this for chaining
   */
  _loadFromCSV(data) {
    try {
      const lines = data.split('\n');

      for (const line of lines) {
        if ((line || '').trim() == '') {
          continue;
        }

        const values = line.split(',');

        if (values.length < 4) {
          throw new Error('Unable to parse CSV');
        }

        const bookISBN = (values[0] || '').trim();
        if (!Book.validateISBN(bookISBN)) {
          throw new Error('Invalid ISBN');
        }

        this._collection[bookISBN] = new Book(bookISBN
            , values[1].trim()
            , values[2].trim()
            , parseInt(values[3].trim()));

        if (this._inventory !== null) {
          this.add(bookISBN, (values.length >= 5) ? values[4].trim() : 0);
        }
      }

      return this;
    } catch (ex) {
      throw ex;
    }
  }

  /**
   * Verify that inventory system has been provided
   * @private
   * @return {boolean} Return true if enable
   * or throw an error
   */
  _checkInventoryEnable() {
    if (!this._inventory) {
      throw new Error('Rental managment disabled. Please provide an Rental management at initialisation.');
    }
    return true;
  }

  /**
   * PUBLIC
   */

  /**
   * Allow to search by ISBN
   * @param {string} ISBN Book ISBN
   * @return {object} Return book in json format or empty object if not found
   */
  search(ISBN) {
    if (!Book.validateISBN(ISBN)) {
      throw new Error('ISBN provided to lookup is invalid ISBN.');
    }

    // Return empty if not found
    if (!this._collection.hasOwnProperty(ISBN)) {
      return {};
    }

    return this._collection[ISBN].toJSON();
  }

  /**
   * Display search on console
   * @param {string} ISBN Book ISBN
   */
  lookup(ISBN) {
    try {
      const result = this.search(ISBN);
      if (this._writer != null) {
        this._writer(`# => ${result.name}, by ${result.author} (${result.published})`);
      }
    } catch (ex) {
      const message=`Unable to find ISBN: ${ISBN}`;

      if (this._writer != null) {
        this._writer(message);
        return;
      }

      throw new Error(message);
    }
  }

  /**
   * Add item to inventory
   * @see Inventory
   * @param {any} item Item to add to inventory
   * @param {number} number The number of copy to add
   * @return {Library} return this for chaining
   * @throws If error if library not set
   */
  add(item, number = 1) {
    this._checkInventoryEnable();

    this._inventory.add(item, number);
    return this;
  }

  /**
   * Borrow a book
   * @param {any} item
   * @return {Library} return this for chaining
   */
  borrow(item) {
    this._checkInventoryEnable();
    try {
      this._inventory.rent(item);
      if (typeof item === 'string' &&
          this._writer !== null) {
        this._writer(`# => Borrow a copy of '${this._collection[item].getName()}'`);
      }
    } catch (ex) {
      throw ex;
    }

    return this;
  }

  /**
   * Return a book
   * @param {any} item
   * @return {Library} return this for chaining
   */
  return(item) {
    this._inventory.return(item);
    if (typeof item==='string'&&
      this._writer!==null) {
      this._writer(`# => Return a copy of '${this._collection[item].getName()}'`);
    }
    return this;
  }

  /**
   * Get stock for item provided.
   * If item not provided, return full stock
   * @see Inventory
   * @param {any} item Optional - Item to search for
   * @return {array|numeric} If no item provide, retrieve list
   * otherwise, provide the stock for the provided item
   */
  stock(item = null) {
    this._checkInventoryEnable();

    const result = this._inventory.stock(item);

    if (Array.isArray(result) &&
    this._writer !== null) {
      for (const book of result) {
        this._writer(`# ${book.reference}, Copies: ${book.own}, Available: ${book.stock}`);
      }
    }
    return result;
  }
}

module.exports = Library;
