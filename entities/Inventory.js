const Book = require('./Book');

/**
 * Handle book stock
 */
class Inventory {
  /**
   * Constructor
   */
  constructor() {
    this._stock = {};
  }

  /**
   * PRIVATE
   */

  /**
    * Retrieve the reference required
    * to identify the item
    * @private
    * @param {any} item
    * @return {string} Reference of the item
    */
  _getItemReference(item) {
    const reference = (item instanceof Book) ? item.getISBN() : item;
    if (typeof reference !== 'string') {
      throw new Error('Item data type provided is not supported.');
    }
    return reference;
  }

  /**
   * Add item to inventory
   * @param {any} item Item to add to inventory
   * @param {number} number The number of copy to add
   * @return {Inventory} return this for chaining
   */
  add(item, number = 1) {
    if (typeof number !== 'number') {
      throw new Error('Expected to provide a number to add');
    }

    try {
      const reference = this._getItemReference(item);

      if (!this._stock.hasOwnProperty(reference)) {
        this._stock[reference] = 0;
      }

      this._stock[reference] += number;
    } catch (ex) {
      throw new Error('Unable to add item into inventory.');
    }

    return this;
  }

  /**
   * Remove item from inventory
   * @param {any} item Item to add to inventory
   * @return {Inventory} Return this for chaining
   */
  deduct(item) {
    const reference = this._getItemReference(item);

    if (!this._stock.hasOwnProperty(reference)) {
      throw new Error('Out of stock');
    }

    if (this._stock[reference] < 1) {
      throw new Error('Out of stock');
    }

    this._stock[reference]--;

    return this;
  }

  /**
   * Get stock for item provided.
   * If item not provided, return full stock
   * @param {any} item Optional - Item to search for
   * @return {array|numeric} If no item provide, retrieve list
   * otherwise, provide the stock for the provided item
   */
  stock(item = null) {
    // If no arguments provided full list
    if (item === null) {
      const items = [];
      for (const key in this._stock) {
        if (this._stock.hasOwnProperty(key)) {
          items.push({
            reference: key,
            stock: this._stock[key],
          });
        }
      }

      return items;
    }

    try {
      const reference = this._getItemReference(item);

      if (!this._stock.hasOwnProperty(reference)) {
        return 0;
      }

      return this._stock[reference];
    } catch (ex) {
      throw new Error('Unable to peform search for item provided.');
    }
  }
}

module.exports = Inventory;
