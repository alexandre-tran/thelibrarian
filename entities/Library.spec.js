const assert = require('assert');
const Library = require('./Library');
const Inventory = require('./Inventory');
const Rental = require('./Rental');

const DUMMY_DATA_ARRAY = Object.freeze([{
  ISBN: '9780143111597',
  name: 'The Left Hand of Darkness',
  author: 'Ursula K. Le Guin',
  published: 1969,
  stock: 5,
}, {
  ISBN: '9781472258229',
  name: 'Kindred',
  author: 'Octavia E. Butler',
  published: 1979,
  stock: 2,
}]);

describe('Library Class Unit test', function() {
  describe('Constructor', function() {
    it('should throws data list type not supported', function() {
      assert.throws(() =>{
        new Library({});
      }, {
        message: 'Library only load data from Array or CSV.',
      });
    });

    it('should throws if one of the item is list not valid ISBN (array)', function() {
      assert.throws(() =>{
        new Library([{
          ISBN: 'abc',
        }]);
      }, {
        message: 'Invalid ISBN',
      });
    });

    it('should throws if one of the item is list not valid ISBN (CSV)', function() {
      assert.throws(() => {
        new Library(`abc,bookname,bookauthor,1981`);
      }, {
        message: 'Invalid ISBN',
      });
    });


    it('should throw error if CSV invalid', function() {
      assert.throws(() => {
        new Library(`abc,bookname,bookauthor`);
      }, {
        message: 'Unable to parse CSV',
      });
    });

    it('should ensure Inventory class type provided', function() {
      assert.throws(() => {
        new Library(DUMMY_DATA_ARRAY, {});
      }, {
        message: 'Rental management must be type Rental',
      });
    });

    it('should construct and load from CSV', function() {
      const data = `9780143111597,The Left Hand of Darkness,Ursula K. Le Guin,1969
      9781472258229,Kindred,Octavia E. Butler,1979
      9780441569595,Neuromancer,William Gibson,1984
      9781857231380,Consider Phlebas,Iain M. Banks,1987
      9780553283686,Hyperion,Dan Simmons,1989`;
      new Library(data);
    });

    it('should skip blank lines when loading from CSV', function() {
      const data = `9780143111597,The Left Hand of Darkness,Ursula K. Le Guin,1969
      9781472258229,Kindred,Octavia E. Butler,1979

      9780441569595,Neuromancer,William Gibson,1984

      9781857231380,Consider Phlebas,Iain M. Banks,1987
      9780553283686,Hyperion,Dan Simmons,1989`;

      new Library(data);
    });

    it('should construct and load from array', function() {
      new Library(DUMMY_DATA_ARRAY);
    });

    it('should ensure that writer is a function', function() {
      assert.throws(() =>{
        new Library(DUMMY_DATA_ARRAY, null, 'writer');
      }, {
        message: 'Writer must be a function callback.',
      });
    });
  });

  describe('Search for book', function() {
    it('should throw error if invalid ISBN', function() {
      const library = new Library([]);
      assert.throws(() =>{
        library.search('abc');
      }, {
        message: 'ISBN provided to lookup is invalid ISBN.',
      });
    });

    it('should allow to search by ISBN', function() {
      // Clone to prevent modifying source
      const expected = Object.assign({}, DUMMY_DATA_ARRAY[0]);
      // Remove parameter that search doesn't provide
      delete expected.stock;

      const library = new Library(DUMMY_DATA_ARRAY);
      const result = library.search(DUMMY_DATA_ARRAY[0].ISBN);
      assert.strictEqual(JSON.stringify(result), JSON.stringify(expected));
    });

    it('should throw error if book not found', function() {
      const library = new Library(DUMMY_DATA_ARRAY);
      const result = library.search('978-1-56619-909-4');
      assert.strictEqual(
          Object.keys(result).length === 0 &&
          result.constructor === Object, true);
    });

    it('should output outcome to own writer', function(done) {
      const data={
        ISBN: '9781472258229',
        name: 'Kindred',
        author: 'Octavia E. Butler',
        published: 1979,
      };

      const library=new Library([data], null, (output) => {
        assert.strictEqual(output, `# => ${data.name}, by ${data.author} (${data.published})`);
        done();
      });

      library.lookup('9781472258229');
      assert.fail('If arrive at this point, writer was not called.');
    });

    it('should indicate ISBN not found to callback', function(done) {
      const notfoundISBN = 'abc';
      const library = new Library([], null, (output) =>{
        assert.strictEqual(output, `Unable to find ISBN: ${notfoundISBN}`);
        done();
      });
      library.lookup(notfoundISBN);
      assert.fail('If arrive at this point, callback was not called.');
    });

    it('should throw error ISBN not found', function() {
      const notfoundISBN='abc';
      const library=new Library([], null);
      assert.throws(() =>{
        library.lookup(notfoundISBN);
      }, {
        message: `Unable to find ISBN: ${notfoundISBN}`,
      });
    });
  });

  describe('Borrow a book', function() {
    it('should output log', function(done) {
      const data=DUMMY_DATA_ARRAY[0];
      const library = new Library([data], new Rental(), (output) =>{
        assert.strictEqual(output, `# => Borrow a copy of '${data.name}'`);
        done();
      });

      library.borrow(data.ISBN);
      assert.fail('If reaching here, then method did not use callback.');
    });
  });

  describe('Return a book', function() {
    it('should output log', function(done) {
      const data=DUMMY_DATA_ARRAY[0];
      const library=new Library([data], new Rental(), (output) => {
        assert.strictEqual(output, `# => Return a copy of '${data.name}'`);
        done();
      });

      library.return(data.ISBN);
      assert.fail('If reaching here, then method did not use callback.');
    });
  });

  describe('Stock of books', function() {
    it('should output stock', function(done) {
      const data=DUMMY_DATA_ARRAY[0];
      const library=new Library([data], new Rental(), (output) => {
        assert.strictEqual(output, `# ${data.ISBN}, Copies: 5, Available: 5`);
        done();
      });

      library.stock();
      assert.fail('If reaching here, then method did not use callback.');
    });
  });

  describe('Inventory', function() {
    it('should throw error when Rental Manager not provided', function() {
      const library = new Library(DUMMY_DATA_ARRAY);
      assert.throws(() => {
        library.add('product 1');
      }, {
        message: 'Rental managment disabled. Please provide an Rental management at initialisation.',
      });

      assert.throws(() => {
        library.stock();
      }, {
        message: 'Rental managment disabled. Please provide an Rental management at initialisation.',
      });
    });

    it('should throw an error when Inventory class provided', function() {
      assert.throws(() => {
        new Library([], new Inventory());
      }, {
        message: 'Rental management must be type Rental',
      });
    });

    it('should list all book available on stock when loaded array', function() {
      const data = [{
        ISBN: '9780143111597',
        name: 'The Left Hand of Darkness',
        author: 'Ursula K. Le Guin',
        published: 1969,
        stock: 1,
      }, {
        ISBN: '9781472258229',
        name: 'Kindred',
        author: 'Octavia E. Butler',
        published: 1979,
        stock: 2,
      }, {
        ISBN: '9780441569595',
        name: 'Neuromancer',
        author: 'William Gibson',
        published: 1984,
        stock: 3,
      }];

      const expected = [{
        reference: '9780143111597',
        stock: 1,
        own: 1,
      }, {
        reference: '9781472258229',
        stock: 2,
        own: 2,
      }, {
        reference: '9780441569595',
        stock: 3,
        own: 3,
      }];

      const library = new Library(data, new Rental());
      assert.strictEqual(JSON.stringify(library.stock())
          , JSON.stringify(expected));
    });

    it('should list all book available on stock when loaded CSV', function() {
      const data = `9780143111597,The Left Hand of Darkness,Ursula K. Le Guin,1969
      9781472258229,Kindred,Octavia E. Butler,1979
      9780441569595,Neuromancer,William Gibson,1984
      9781857231380,Consider Phlebas,Iain M. Banks,1987
      9780553283686,Hyperion,Dan Simmons,1989`;

      const library = new Library(data, new Rental());
      assert.strictEqual(library.stock().length, data.split('\n').length);
    });

    it('should add and get book stock', function() {
      const data = [{
        reference: 'Book1',
        stock: 5,
        own: 5,
      }, {
        reference: 'Book2',
        stock: 2,
        own: 2,
      }];

      const library = new Library([], new Rental());

      for (const book of data) {
        library.add(book.reference, book.stock);
      }

      assert.strictEqual(JSON.stringify(library.stock('Book1')), JSON.stringify(data[0]));
      assert.strictEqual(JSON.stringify(library.stock('Book2')), JSON.stringify(data[1]));
      assert.strictEqual(JSON.stringify(library.stock()), JSON.stringify(data), 'Expected to have same dataset');
    });

    it('should allow to borrow book', function() {
      const bookISBN = '9781472258229';
      const library = new Library(DUMMY_DATA_ARRAY, new Rental());
      library.borrow(bookISBN);
      assert.strictEqual(library.stock(bookISBN).stock, 1);
    });

    it('should prevent from borrowing a book if no available', function() {
      const bookISBN = '9781472258229';
      const library = new Library(DUMMY_DATA_ARRAY, new Rental());
      assert.throws(() => {
        library.borrow(bookISBN)
            .borrow(bookISBN)
            .borrow(bookISBN);
      }, {
        message: 'Out of stock',
      });
    });

    it('should add back when book return', function() {
      const bookISBN = '9781472258229';
      const library = new Library(DUMMY_DATA_ARRAY, new Rental());
      library.return(bookISBN);
      assert.strictEqual(library.stock(bookISBN).stock, 3);
    });
  });

  describe('Rental', function() {
    describe('Stock', function() {
      it('should list all book registered', function() {
        const data = [{
          ISBN: '9780143111597',
          name: 'The Left Hand of Darkness',
          author: 'Ursula K. Le Guin',
          published: 1969,
        }, {
          ISBN: '9781472258229',
          name: 'Kindred',
          author: 'Octavia E. Butler',
          published: 1979,
        }];

        const library = new Library(data, new Rental());
        assert.strictEqual(library.stock().length, data.length);
      });

      it('Worfklow renting and returning back', function() {
        const data = {
          ISBN: '9780441569595',
          name: 'Neuromancer',
          author: 'William Gibson',
          published: 1984,
        };

        const library = new Library([data], new Rental());

        library.add(data.ISBN, 3)
            .borrow(data.ISBN)
            .borrow(data.ISBN)
            .return(data.ISBN);

        const item = library.stock(data.ISBN);
        assert.strictEqual(item.stock, 2);
        assert.strictEqual(item.reference, data.ISBN);
        assert.strictEqual(item.own, 3);
      });
    });
  });
});
