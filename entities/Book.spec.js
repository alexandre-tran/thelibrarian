const assert=require('assert');
const Book=require('./Book');
describe('Book Class Unit test', function() {
  describe('Book properties validation', function() {
    it('should ensure ISBN valid', function() {
      assert.strictEqual(Book.validateISBN('9780143111597'), true);
      assert.strictEqual(Book.validateISBN('978-1-56619-909-4'), true);
      assert.strictEqual(Book.validateISBN('978-1-56619-909-4 2'), false);
      assert.strictEqual(Book.validateISBN('1-56619-909-3'), true);
      assert.strictEqual(Book.validateISBN('isbn446877428ydh'), false);
      assert.strictEqual(Book.validateISBN('55 65465 4513574'), false);
      assert.strictEqual(Book.validateISBN('1257561035'), true);
      assert.strictEqual(Book.validateISBN('1248752418865'), true);
    });
  });

  describe('Constructor', function() {
    describe('Required parameters', function() {
      it('should require ISBN, name, author, year published', function() {
        assert.throws(() => {
          new Book();
        }, {
          message: 'Book expect a ISBN, name, author and year published.',
        });
      });
    });

    describe('ISBN Validation', function() {
      it('should construct with valid ISBN', function() {
        new Book('9780143111597'
            , 'The Left Hand of Darkness'
            , 'Ursula K. Le Guin'
            , 1969);
      });

      it('should not allow to construct with invalid ISBN', function() {
        assert.throws(() => {
          new Book('978-1-56619-909-4 2'
              , 'Unexpected title'
              , 'Creative mind'
              , 1982);
        }, {
          message: 'Invalid ISBN.',
        });
      });
    });

    describe('Published Validation', function() {
      it('should only accept a numeric year', function() {
        assert.throws(() => {
          new Book('9780143111597'
              , 'The Left Hand of Darkness'
              , 'Ursula K. Le Guin'
              , '1969');
        }, {
          message: 'Published must a be numeric year.',
        });
      });
    });
  });

  describe('Getter', function() {
    it('should allow to retrieve the ISBN', function() {
      const bookISBN='9780143111597';
      const book=new Book(bookISBN
          , 'The Left Hand of Darkness'
          , 'Ursula K. Le Guin'
          , 1969);

      assert.strictEqual(book.getISBN()
          , bookISBN
          , 'Expected to have getter provide book ISBN');
    });
  });

  describe('toJson', function() {
    it('should return JSON object', function() {
      const data={
        ISBN: '9780143111597',
        name: 'The Left Hand of Darkness',
        author: 'Ursula K. Le Guin',
        published: 1969,
      };

      const book=new Book(data.ISBN
          , data.name
          , data.author
          , data.published);

      assert.strictEqual(JSON.stringify(book.toJSON()), JSON.stringify(data));
    });
  });
});
