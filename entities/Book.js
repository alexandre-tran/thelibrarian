/**
 * Book class to hold book information
 */
class Book {
  /**
   * Constructor
   * @param {string}  ISBN      Book number identity
   * @param {string}  name      The name of the book
   * @param {string}  author    name of the author
   * @param {number}  published The year published
   */
  constructor(ISBN, name, author, published) {
    if (ISBN === undefined ||
      name === undefined ||
      author === undefined ||
      published === undefined) {
      throw new Error('Book expect a ISBN, name, author and year published.');
    }

    // ISBN must be a valid one
    if (!this.constructor.validateISBN(ISBN)) {
      throw new Error('Invalid ISBN.');
    }

    // Only accept numeric value
    if (!Number.isInteger(published)) {
      throw new Error('Published must a be numeric year.');
    }

    this._ISBN = ISBN;
    this._name = name;
    this._author = author;
    this._published = published;
  }

  /** *************
   * STATICS
   **************/

  /**
   * ISBN validation
   * @param {string} value ISBN to validate
   * @return {boolean}
   */
  static validateISBN(value) {
    const regex = /^(?=(?:\D*\d){10}(?:(?:\D*\d){3})?$)[\d-]+$/;
    return regex.test(value); ;
  }

  /**
   * GETTER
   */

  /**
   * Getter for ISBN
   * @return {string} Book ISBN
   */
  getISBN() {
    return this._ISBN;
  }

  /**
   * Get book name
   * @return {string} Book name
   */
  getName() {
    return this._name;
  }

  /**
   * PUBLIC
   */

  /**
   * Return book in JSON format
   * @return {JSON} Book information in JSON format
   */
  toJSON() {
    return {
      ISBN: this._ISBN,
      name: this._name,
      author: this._author,
      published: this._published,
    };
  }
}

module.exports = Book;
