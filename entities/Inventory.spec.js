const assert = require('assert');
const Book = require('./Book');
const Inventory = require('./Inventory');

describe('Inventory Class Unit Test', function() {
  describe('Check the stock method', function() {
    it('should throw error if item type not supported', function() {
      const inventory = new Inventory();
      assert.throws(() =>{
        inventory.stock({});
      }, {
        message: 'Unable to peform search for item provided.',
      });
    });

    it('should return 0 if item not found', function() {
      const inventory = new Inventory();
      assert.strictEqual(inventory.stock('my item'), 0
          , 'Expected to return 0 on no existing item');
    });

    it('should allow to get stock from Book object', function() {
      const book = new Book('9780143111597'
          , 'The Left Hand of Darkness'
          , 'Ursula K. Le Guin'
          , 1969);

      const inventory = new Inventory();
      assert.strictEqual(inventory.stock(book), 0
          , 'Expected to return 0 on no existing item');
    });
  });

  describe('Add to stock method', function() {
    it('should throw error if unsupported item type', function() {
      const inventory = new Inventory();
      assert.throws(() =>{
        inventory.add({});
      }, {
        message: 'Unable to add item into inventory.',
      });
    });

    it('should allow to add via ISBN', function() {
      const bookISBN = '9780143111597';
      const inventory = new Inventory();
      inventory.add(bookISBN);
      assert.strictEqual(inventory.stock(bookISBN), 1
          , 'Did not provide the right amount');
    });

    it('should allow to add multiple items', function() {
      const bookISBN = '9780143111597';
      const numberOfCopies = 5;
      const inventory = new Inventory();
      inventory.add(bookISBN, numberOfCopies);
      assert.strictEqual(inventory.stock(bookISBN), numberOfCopies
          , `Number of copies not matching`);
    });

    it('should only allow to provide a number', function() {
      const bookISBN = '9780143111597';
      const inventory = new Inventory();
      assert.throws(() => {
        inventory.add(bookISBN, '5');
      }, {
        message: 'Expected to provide a number to add',
      });
    });

    it('should provide the ability to add a book', function() {
      const book = new Book(
          '9780143111597'
          , 'The Left Hand of Darkness'
          , 'Ursula K. Le Guin'
          , 1969,
      );

      const inventory = new Inventory();
      inventory.add(book);

      assert.strictEqual(inventory.stock(book.getISBN()), 1
          , 'Expected to added appropriately to the stock');
    });
  });

  describe('Deduct from stock method', function() {
    it('should deduct from inventory', function() {
      const inventory = new Inventory();
      inventory.add('Item 1', 5)
          .deduct('Item 1');

      assert.strictEqual(inventory.stock('Item 1'), 4);
    });

    const book = new Book(
        '9780143111597'
        , 'The Left Hand of Darkness'
        , 'Ursula K. Le Guin'
        , 1969,
    );

    const inventory = new Inventory();
    inventory.add(book)
        .deduct(book);

    assert.strictEqual(inventory.stock(book.getISBN()), 0
        , 'Expected to added appropriately to the stock');
  });

  describe('Stock management', function() {
    it('should provide full list of item', function() {
      const data = [{
        reference: 'Product 1',
        stock: 5,
      }, {
        reference: 'Product 2',
        stock: 3,
      }];
      const inventory = new Inventory();
      for (const product of data) {
        inventory.add(product.reference, product.stock);
      }

      assert.strictEqual(JSON.stringify(inventory.stock()), JSON.stringify(data)
          , 'Expected stock to match data');
    });

    it('should not allow to deduct if out of stock', function() {
      const inventory = new Inventory();

      assert.throws(() => {
        inventory.deduct('Product 1');
      }, {
        message: 'Out of stock',
      });

      assert.throws(() => {
        const productName = 'Product 2';
        inventory.add(productName, 1)
            .deduct(productName)
            .deduct(productName);
      }, {
        message: 'Out of stock',
      });
    });
  });
});
