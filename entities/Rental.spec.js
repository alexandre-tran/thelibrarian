const assert = require('assert');
const Rental = require('./Rental');

describe('Rental Class Unit Testing', function() {
  describe('Stock', function() {
    it('should provide stock & availibility', function() {
      const data = {
        reference: 'Item1',
        stock: 0,
        own: 0,
      };

      const rental = new Rental();
      assert.strictEqual(JSON.stringify(rental.stock(data.reference))
          , JSON.stringify(data));
    });
  });

  describe('Add items', function() {
    it('should throw error if fail to add', function() {
      const rental = new Rental();
      assert.throws(() =>{
        rental.add({});
      }, {
        message: 'Unable to add item to stock',
      });
    });

    it('should keep track of single item added', function() {
      const data = {
        reference: 'item1',
        stock: 1,
        own: 1,
      };

      const rental = new Rental();
      rental.add(data.reference);
      assert.strictEqual(JSON.stringify(rental.stock(data.reference)), JSON.stringify(data), 'Expected to match stock');
    });

    it('should keep track of total own', function() {
      const data = {
        reference: 'Item1',
        stock: 5,
        own: 5,
      };

      const rental = new Rental();
      rental.add(data.reference, data.own);
      assert.strictEqual(JSON.stringify(rental.stock(data.reference)), JSON.stringify(data), 'Expected to match stock');
    });
  });

  describe('Search item', function() {
    it('should throw error if unable to find item', function() {
      const rental = new Rental();
      assert.throws(() =>{
        rental.stock({});
      }, {
        message: 'Unable to peform search for item provided',
      });
    });
  });

  describe('Rent an item', function() {
    it('should deduct from stock', function() {
      const data = {
        reference: 'Item1',
        stock: 3,
        own: 3,
      };

      const rental = new Rental();

      rental.add(data.reference, data.own)
          .rent(data.reference)
          .rent(data.reference);

      assert.strictEqual(rental.stock(data.reference).stock, 1);
    });

    it('should add to stock when return', function() {
      const data = {
        reference: 'Item1',
        stock: 3,
        own: 3,
      };

      const rental = new Rental();

      rental.add(data.reference, data.own)
          .rent(data.reference)
          .return(data.reference);

      assert.strictEqual(rental.stock(data.reference).stock, 3);
    });

    it('should add to own when returned more then own', function() {
      const data = {
        reference: 'Item1',
        stock: 3,
        own: 3,
      };

      const rental = new Rental();

      rental.add(data.reference, data.own)
          .return(data.reference);

      assert.strictEqual(rental.stock(data.reference).stock, 4);
      assert.strictEqual(rental.stock(data.reference).own, 4);
    });
  });

  describe('Rental Stock management', function() {
    it('should provide accurate stock', function() {
      const data = [{
        reference: 'item1',
        stock: 5,
        own: 5,
      }, {
        reference: 'item2',
        stock: 3,
        own: 3,
      }];

      const rental = new Rental();

      for (const item of data) {
        rental.add(item.reference, item.own);
      }

      rental.deduct(data[1].reference)
          .deduct(data[1].reference);

      const expected1 = {
        reference: data[1].reference,
        stock: 1,
        own: 3,
      };

      assert.strictEqual(JSON.stringify(rental.stock(data[1].reference))
          , JSON.stringify(expected1));

      const expected2 = [...data];
      expected2[1] = expected1;
      assert.strictEqual(JSON.stringify(rental.stock())
          , JSON.stringify(expected2));
    });
  });
});
