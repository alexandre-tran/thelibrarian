const https = require('https');
const Library = require('./entities/Library');
const Rental = require('./entities/Rental');

process.env = {};

for (const arg of process.argv) {
  if (arg.includes('=')) {
    const config = arg.split('=');
    process.env[config[0]] = config[1];
  }
}

// Load from Github
const options = {
  hostname: 'raw.githubusercontent.com',
  path: '/gar/library_code_challenge/main/catalog.csv',
  method: 'GET',
};

const run = function(data) {
  const corkCity = new Library(data
      , new Rental()
      , (process.env.roleplay ? console.log : null));

  corkCity.lookup('9781472258229');
  corkCity.add('9781472258229');

  corkCity.lookup('9780441569595');
  corkCity.add('9780441569595', 3);

  corkCity.borrow('9781472258229');
  corkCity.borrow('9780441569595');
  corkCity.borrow('9780441569595');
  corkCity.return('9780441569595');

  corkCity.stock();
};

const request = https.request(options, (response) => {
  let result = '';

  response.on('data', (data) => {
    result += data;
  });

  response.on('end', () => {
    run(result);
  });
}).end();

request.on('error', (error) => {
  console.error(error);
});

